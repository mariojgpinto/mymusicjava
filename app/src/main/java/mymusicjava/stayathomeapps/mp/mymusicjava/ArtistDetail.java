package mymusicjava.stayathomeapps.mp.mymusicjava;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class ArtistDetail extends AppCompatActivity {
    ImageView image;
    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_detail);

        image = findViewById(R.id.image_artistDetail);
        text = findViewById(R.id.text_artistDetail);

        Picasso.get()
                .load("https://lastfm-img2.akamaized.net/i/u/174s/20d9729683904ee499f6079f0de188d3.png")
                .placeholder(R.drawable.album_default)
                .error(R.drawable.album_error)
                .into(image);

        text.setText(getIntent().getStringExtra("ARTIST"));
    }
}
