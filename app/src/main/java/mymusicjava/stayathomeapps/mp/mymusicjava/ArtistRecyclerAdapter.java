package mymusicjava.stayathomeapps.mp.mymusicjava;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by mario on 05/06/2018
 */
public class ArtistRecyclerAdapter extends RecyclerView.Adapter<ArtistRecyclerAdapter.AlbumViewHolder> {
    private String[] artists;

    private Context context;



    public ArtistRecyclerAdapter(List<String> _albuns, Context _context){
        this.artists = (String[])_albuns.toArray();
        this.context = _context;
    }
    //GridLayout grid;

    /**
     * Responsible for create each object of the Viewholder (in this example, AlbumViewHolder).
     * @param parent
     * @param viewType
     * @return An ViewHolder object (AlbumViewHoder)
     */
    @NonNull
    @Override
    public AlbumViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.artist_layout, parent, false);

        AlbumViewHolder element = new AlbumViewHolder(view, context, artists);

        return element;
    }

    /**
     * Places the data on the ViewHolder components.
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(@NonNull AlbumViewHolder holder, int position) {
        holder.text.setText(this.artists[position]);

        Picasso.get()
                .load("https://lastfm-img2.akamaized.net/i/u/174s/20d9729683904ee499f6079f0de188d3.png")
                .placeholder(R.drawable.album_default)
                .error(R.drawable.album_error)
                .into(holder.image);


//        holder.image.setImageResource(R.drawable.album_default);
    }

    @Override
    public int getItemCount() {
        return artists.length;
    }

    public static class AlbumViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView image;
        TextView text;
        Context context;
        String[] artists;

        public AlbumViewHolder(View itemView, Context _context, String[] _artists) {
            super(itemView);

            this.image = itemView.findViewById(R.id.album_image);
            this.text = itemView.findViewById(R.id.album_name);
            this.context = _context;
            this.artists = _artists;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, ArtistDetail.class);
            intent.putExtra("ARTIST", artists[getAdapterPosition()]);

            context.startActivity(intent);
        }
    }
}
