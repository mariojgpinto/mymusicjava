package mymusicjava.stayathomeapps.mp.mymusicjava;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;

    private List<String> list;

    TextRecyclerAdapter textRecyclerAdapter;

    ArtistRecyclerAdapter albumRecyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerView);

        layoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(layoutManager);

        list = Arrays.asList(getResources() .getStringArray(R.array.default_artist));

//        textRecyclerAdapter = new TextRecyclerAdapter(list);
//
//        recyclerView.setHasFixedSize(true);
//        recyclerView.setAdapter(textRecyclerAdapter);

        albumRecyclerAdapter = new ArtistRecyclerAdapter(list, this);
        recyclerView.setAdapter(albumRecyclerAdapter);

    }
}
