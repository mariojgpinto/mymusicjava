package mymusicjava.stayathomeapps.mp.mymusicjava;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by mario on 03/06/2018
 */

public class TextRecyclerAdapter extends RecyclerView.Adapter<TextRecyclerAdapter.MyTextViewHolder>{
    private List<String> list;

    public TextRecyclerAdapter(List<String> _list){
        this.list = _list;
    }

    @NonNull
    @Override
    public MyTextViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        TextView textView = (TextView)LayoutInflater.from(parent.getContext()).inflate(R.layout.text_view_layout, parent, false);
        MyTextViewHolder myViewHolder = new MyTextViewHolder(textView);

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyTextViewHolder holder, int position) {
        holder.artistName.setText(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class MyTextViewHolder extends RecyclerView.ViewHolder
    {
        TextView artistName;

        public MyTextViewHolder(TextView itemView) {
            super(itemView);

            artistName = itemView;
        }
    }
}
